# CCSG Power Library

This library offers functions that abstract the power management for EnviSense boards.  Each subsystem with a load switch (power switch) has a function that supports putting that subsystem into various states.
