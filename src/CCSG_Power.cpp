/*******************************************************************************
 * CCSG Board Support
 *
 * EnviSense Power Management
 *
 * Copyright (c) 2019, 2020 by Bob Iannucci
 *******************************************************************************/

extern "C" {
#include <string.h>
}

#include "CCSG_Power.h"
#include "CCSG_Debug.h"

#ifdef __cplusplus
extern "C" {
#endif

ccsg_power_state_t ccsg_power_state =
{
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
#if ENVISENSE_SERIAL == ENVISENSE_BLUETOOTH
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
#elif ENVISENSE_SERIAL == ENVISENSE_DEBUG
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
#endif
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN},
	{ POWER_STATE_UNKNOWN, POWER_STATE_UNKNOWN}
};

// --------------------------------------- Internal functions ---------------------------------------


// Typical:
//   pPort					PIOA, PIOB, PIOC, PIOD
//   ulPinType				PIO_PERIPH_A, PIO_PERIPH_B, PIO_INPUT, PIO_OUTPUT_0, PIO_OUTPUT_1
//   ulPin					PIO_PA8A_UXRD
//   ulPinConfiguration		PIO_DEFAULT
static void _configure_pin(uint8_t pin)
{
	PIO_Configure(
			g_APinDescription[pin].pPort,
			g_APinDescription[pin].ulPinType,
			g_APinDescription[pin].ulPin,
			g_APinDescription[pin].ulPinConfiguration);
}


static void _configure_pin_t(uint8_t pin, EPioType ulPinType)
{
	PIO_Configure(
			g_APinDescription[pin].pPort,
			ulPinType,
			g_APinDescription[pin].ulPin,
			g_APinDescription[pin].ulPinConfiguration);
}


static void _configure_pin_tc(uint8_t pin, EPioType ulPinType, uint32_t ulPinConfiguration)
{
	PIO_Configure(
			g_APinDescription[pin].pPort,
			ulPinType,
			g_APinDescription[pin].ulPin,
			ulPinConfiguration);
}


// Iterate through a range of pins -- for each one configured as a GPIO,
// set the hardware to input or output, with or without a pullup (if input)
// and at the correct value (if output).  Ignore other pin types.
// static void _configure_pins(uint8_t fromPin, uint8_t toPin)
// {
// 	int8_t pin;
// //	EPioType pinType;
// //	uint32_t pinConfiguration;

// 	for (pin=fromPin; pin<=toPin; pin++)  // N.B. INCLUSIVE of the to_pin
// 	{
// 		_configure_pin(pin);
// 	}
// }


void _set_power_leds(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false; 

	switch (action)
	{
	case ACTION_RESET:
		_configure_pin(LED_GREEN);
		_configure_pin(LED_RED);
		break;
	case ACTION_ENABLE:
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		_configure_pin_t(LED_GREEN, PIO_OUTPUT_1);
		_configure_pin_t(LED_RED, PIO_OUTPUT_1);
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		_configure_pin_t(LED_GREEN, PIO_OUTPUT_1);
		_configure_pin_t(LED_RED, PIO_OUTPUT_1);
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{

		}
		break;
	default:
		break;
	}
}


void _set_power_accelerometer(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;

	switch (action)
	{
	case ACTION_RESET:
		_configure_pin(ACCL_INT1);
		_configure_pin(ACCL_INT2);
		break;
	case ACTION_ENABLE:
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		_configure_pin_t(ACCL_INT1, PIO_OUTPUT_0);
		_configure_pin_t(ACCL_INT2, PIO_OUTPUT_0);
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		_configure_pin_t(ACCL_INT1, PIO_OUTPUT_0);
		_configure_pin_t(ACCL_INT2, PIO_OUTPUT_0);
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{

		}
		break;
	default:
		break;
	}
}


void _set_power_humidity_sensor(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	switch (action)
	{
	case ACTION_RESET:
		break;
	case ACTION_ENABLE:
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{

		}
		break;
	default:
		break;
	}
}


// **FIXME**
void _set_power_lora(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	switch (action)
	{
	case ACTION_RESET:
		_configure_pin(LORA_DIO0);
		_configure_pin(LORA_DIO1);
		_configure_pin(LORA_CS);
		_configure_pin(LORA_RESET);
//		_configure_pin(LORA_EN);  12/19/19
		_configure_pin_tc(LORA_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		break;
	case ACTION_ENABLE:
		_configure_pin_tc(LORA_EN, PIO_OUTPUT_1, PIO_DEFAULT);
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		_configure_pin_tc(LORA_EN, PIO_OUTPUT_0, PIO_DEFAULT);  // 12/19/19
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:   // Can't copy ALT configuration because the LoRa IC needs to remain live
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		_configure_pin_t(LORA_DIO0, PIO_OUTPUT_0);
		_configure_pin_t(LORA_DIO1, PIO_OUTPUT_0);
		_configure_pin_t(LORA_CS, PIO_OUTPUT_0);  // Even though neg active, the chip is unpowered, so we feed it 0V
		_configure_pin_t(LORA_RESET, PIO_OUTPUT_0);  // Even though neg active, the chip is unpowered, so we feed it 0V
		_configure_pin_t(LORA_EN, PIO_OUTPUT_0);
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{

		}
		break;
	default:
		break;
	}
}


void _set_power_coulomb_counter(gpio_action_t action)
{
#if defined(BOARD_ENVISENSE22) || defined(BOARD_ENVISENSE23) || defined(BOARD_ENVISENSE24) // No CC on EnviSense20
	static bool enabled_p __attribute__((unused)) = false;
	switch (action)
	{
	case ACTION_RESET:
		_configure_pin_t(EXP_I2C_SDA, PIO_INPUT);
		_configure_pin_t(EXP_I2C_SCL, PIO_INPUT);
		enabled_p = false;
		break;
	case ACTION_ENABLE:
		CCSG_DEBUG_PRINTLN("[power   ] Enabling coulomb counter I2C");
		_configure_pin_t(EXP_I2C_SDA, PIO_PERIPH_A);
		_configure_pin_t(EXP_I2C_SCL, PIO_PERIPH_A);
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		CCSG_DEBUG_PRINTLN("[power   ] Disabling coulomb counter I2C");
		_configure_pin_t(EXP_I2C_SDA, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		_configure_pin_t(EXP_I2C_SCL, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		_configure_pin_t(EXP_I2C_SDA, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		_configure_pin_t(EXP_I2C_SCL, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		_configure_pin_t(EXP_I2C_SDA, PIO_INPUT);  // **FOR BOARDS WITH I2C PULLUP SWITCHING**
		_configure_pin_t(EXP_I2C_SCL, PIO_INPUT);  // **FOR BOARDS WITH I2C PULLUP SWITCHING**
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{
			_configure_pin_t(EXP_I2C_SDA, PIO_PERIPH_A);
			_configure_pin_t(EXP_I2C_SCL, PIO_PERIPH_A);
		}
		break;
	default:
		break;
	}
#endif
}


void _set_power_i2c(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	switch (action)
	{
	case ACTION_RESET:
		_configure_pin_t(I2C_SDA, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		_configure_pin_t(I2C_SCL, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		_configure_pin(I2C_EN);  // BLE_EN borrowed for I2Cdomain
		enabled_p = false;
		break;
	case ACTION_ENABLE:
		_configure_pin_t(I2C_EN, PIO_OUTPUT_1);
		_configure_pin_t(I2C_SDA, PIO_PERIPH_A);
		_configure_pin_t(I2C_SCL, PIO_PERIPH_A);
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		_configure_pin_t(I2C_SDA, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		_configure_pin_t(I2C_SCL, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		_configure_pin(I2C_EN);
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		_configure_pin_t(I2C_SDA, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		_configure_pin_t(I2C_SCL, PIO_INPUT);  // Prefer PIO_OUTPUT_0, but not all boards have the I2C load switch to turn off the pullup
		_configure_pin_t(I2C_EN, PIO_OUTPUT_0);
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		_configure_pin_t(I2C_SDA, PIO_OUTPUT_0);  // **FOR BOARDS WITH I2C PULLUP SWITCHING**
		_configure_pin_t(I2C_SCL, PIO_OUTPUT_0);  // **FOR BOARDS WITH I2C PULLUP SWITCHING**
		_configure_pin_t(I2C_EN, PIO_OUTPUT_0);   // **FOR BOARDS WITH I2C PULLUP SWITCHING**
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{
			_configure_pin_t(I2C_EN, PIO_OUTPUT_1);
			_configure_pin_t(I2C_SDA, PIO_PERIPH_A);
			_configure_pin_t(I2C_SCL, PIO_PERIPH_A);
		}
		break;
	default:
		break;
	}
}


#if ENVISENSE_SERIAL == ENVISENSE_BLUETOOTH
// WARNING: shares hardware with debug!!
//
// Shared port that requires enabling a load switch for BLE mode.
// Also, if Debug mode, DO NOT disable the pins, ever.
void _set_power_bluetooth(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	uint32_t rxPin = PIO_PA8;
	uint32_t txPin = PIO_PA9;

	uint32_t arduinoRxPin = BLE_COMM_RXD;
	uint32_t arduinoTxPin = BLE_COMM_TXD;

	// ASSUMPTIONS
	//  The BLE module is installed and is being used
	//  Debugging is NOT being done via this port
	//  Power switching and I/O switching should be driven solely by the Bluetooth driver
	switch (action)
	{
	case ACTION_RESET:
		_configure_pin_tc(arduinoRxPin, PIO_OUTPUT_0, PIO_DEFAULT);      // Rx // OK 10.12.19
		_configure_pin_tc(arduinoTxPin, PIO_OUTPUT_0, PIO_DEFAULT);      // Tx  // OK 10.12.19
		_configure_pin_tc(BLE_INT, PIO_OUTPUT_0, PIO_DEFAULT);    // Bluetooth interrupt -- hold this pin low
		_configure_pin_tc(BLE_NRESET, PIO_OUTPUT_0, PIO_DEFAULT); // Bluetooth reset -- hold this pin low
		_configure_pin_tc(BLE_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		break;
	case ACTION_ENABLE:
		_configure_pin_tc(arduinoRxPin, PIO_PERIPH_A, PIO_DEFAULT);      // Rx  // OK 10.12.19
		_configure_pin_tc(arduinoTxPin, PIO_PERIPH_A, PIO_DEFAULT);      // Tx  // OK 10.12.19
		_configure_pin_tc(BLE_INT, PIO_INPUT, PIO_DEFAULT);       // Bluetooth interrupt
		_configure_pin_tc(BLE_NRESET, PIO_OUTPUT_1, PIO_DEFAULT); // Bluetooth reset -- hold this pin high when module is powered on
		_configure_pin_tc(BLE_EN, PIO_OUTPUT_1, PIO_DEFAULT);
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		_configure_pin_tc(BLE_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		_configure_pin_tc(BLE_INT, PIO_OUTPUT_0, PIO_DEFAULT);    // Bluetooth interrupt -- hold this pin low
		_configure_pin_tc(BLE_NRESET, PIO_OUTPUT_0, PIO_DEFAULT); // Bluetooth reset -- hold this pin low
		UART->UART_CR = UART_CR_RSTRX | UART_CR_RSTTX | UART_CR_RXDIS | UART_CR_TXDIS | UART_CR_RSTSTA;
		PIOA->PIO_ODR = rxPin;
		PIOA->PIO_PER = rxPin;
		PIOA->PIO_SODR = txPin;         // OK 10.12.19
		PIOA->PIO_OER = txPin;
		PIOA->PIO_PER = txPin;
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		// **WARNING** driver controls enabling / disabling and should have left pins in the disabled state
		if (enabled_p)
		{
			Serial.println(F("*** WARNING: attempt to sleep with Bluetooth enabled"));
		}
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		// **WARNING** driver controls enabling / disabling and should have left pins in the disabled state
		if (enabled_p)
		{
			Serial.println(F("*** WARNING: attempt to sleep with Bluetooth enabled"));
		}
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{

		}
		break;
	default:
		break;
	}
}
#endif


#if ENVISENSE_SERIAL == ENVISENSE_DEBUG
// WARNING: shares hardware with bluetooth!!
//
// Shared port that requires enabling a load switch for BLE mode.
// Also, if Debug mode, DO NOT disable the pins, ever.
void _set_power_debug(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	uint32_t rxPin = PIO_PA8;
	uint32_t txPin = PIO_PA9;

	uint32_t arduinoRxPin = DEBUG_COMM_RXD;
	uint32_t arduinoTxPin = DEBUG_COMM_TXD;
	// ASSUMPTIONS
	//  The BLE module is NOT installed
	//  Debugging is being done via this port
	//  Power switching is handled by the I2C driver
	switch (action)
	{
	case ACTION_RESET:
		// BLE_EN is handled by I2C
		_configure_pin_tc(arduinoRxPin, PIO_OUTPUT_0, PIO_DEFAULT);      // Rx  // OK 10.12.19
		_configure_pin_tc(arduinoTxPin, PIO_OUTPUT_0, PIO_DEFAULT);      // Tx  // OK 10.12.19
#if defined(BOARD_ENVISENSE20)
		_configure_pin_tc(BLE_INT, PIO_OUTPUT_0, PIO_DEFAULT);    // Bluetooth interrupt -- hold this pin low -- not connected
		_configure_pin_tc(BLE_NRESET, PIO_OUTPUT_0, PIO_DEFAULT); // Bluetooth reset -- hold this pin low -- not connected
#endif
		break;
	case ACTION_ENABLE:
		_configure_pin_tc(arduinoRxPin, PIO_PERIPH_A, PIO_DEFAULT);      // Rx  // OK 10.12.19  was PIO_OUTPUT_0, chgd 2020-1011 to support command interpreter
		_configure_pin_tc(arduinoTxPin, PIO_PERIPH_A, PIO_DEFAULT);      // Tx  // OK 10.12.19
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		// Reset Tx and Rx state machines; disable them; reset status bits
		USART0->US_CR = UART_CR_RSTRX | UART_CR_RSTTX | UART_CR_RXDIS | UART_CR_TXDIS | UART_CR_RSTSTA;
		PIOA->PIO_ODR = rxPin;                                        // Rx - set this pin as input (it is connected to an FTDI IC)
		PIOA->PIO_PER = rxPin;
		PIOA->PIO_SODR = txPin;            // OK 10.12.19 -- Tx - set this pin high (it is connected to an FTDI IC)
		PIOA->PIO_OER = txPin;
		PIOA->PIO_PER = txPin;
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		// OK to sleep with debug port enabled
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		// OK to sleep with debug port enabled
		break;
	case ACTION_WAKE_FROM_SLEEP:
		// Never any action here -- the driver handles explicit enabling and disabling
		break;
	default:
		break;
	}
}
#endif


// USART0 / Serial1: Ultrasonic
void _set_power_ultrasonic(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	uint32_t rxPin = PIO_PA10;
	uint32_t txPin = PIO_PA11;

	uint32_t arduinoRxPin = ULTRASONIC_COMM_RXD;
	uint32_t arduinoTxPin = ULTRASONIC_COMM_TXD;

	switch (action)
	{
	case ACTION_RESET:
		_configure_pin_tc(arduinoRxPin, PIO_OUTPUT_0, PIO_DEFAULT);  // Rx  // OK 10.12.19
		_configure_pin_tc(arduinoTxPin, PIO_OUTPUT_0, PIO_DEFAULT);  // Force Tx low always (it is an input to the inverter IC whose power is toggled)
		_configure_pin_tc(SONAR_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		break;
	case ACTION_ENABLE:                           // Peripheral control
//		CCSG_DEBUG_PRINTLN("[power   ] Enabling arduinoRxPin");
		_configure_pin_tc(arduinoRxPin, PIO_PERIPH_A, PIO_DEFAULT);  // Rx  // OK 10.12.19
//		CCSG_DEBUG_PRINTLN("[power   ] Enabling arduinoTxPin");
		_configure_pin_tc(arduinoTxPin, PIO_PERIPH_A, PIO_DEFAULT);  // Tx  // OK 10.12.19
//		CCSG_DEBUG_PRINTLN("[power   ] Enabling SONAR_EN");
		_configure_pin_tc(SONAR_EN, PIO_OUTPUT_1, PIO_DEFAULT);
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		// Reset Tx and Rx state machines; disable them; reset status bits
		USART0->US_CR = UART_CR_RSTRX | UART_CR_RSTTX | UART_CR_RXDIS | UART_CR_TXDIS | UART_CR_RSTSTA;
		PIOA->PIO_CODR = rxPin;                                   // Rx - this DOES set the pin low
		PIOA->PIO_OER = rxPin;
		PIOA->PIO_PER = rxPin;
		PIOA->PIO_CODR = txPin;                                   // Tx - this DOES set the pin low  // OK 10.12.19
		PIOA->PIO_OER = txPin;
		PIOA->PIO_PER = txPin;
		_configure_pin_tc(SONAR_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		// OK to sleep with ultrasonic enabled
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		// OK to sleep with ultrasonic enabled
		break;
	case ACTION_WAKE_FROM_SLEEP:
		// Never any action here -- the driver handles explicit enabling and disabling
		break;
	default:
		break;
	}
}


// USART1 / Serial2 / GPS
void _set_power_gps(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	uint32_t rxPin = PIO_PA12;
	uint32_t txPin = PIO_PA13;

	uint32_t arduinoRxPin = GPS_COMM_RXD;
	uint32_t arduinoTxPin = GPS_COMM_TXD;

	switch (action)
	{
	case ACTION_RESET:
		// The following statement, if compiled in or left out, has an idle current
		// of 100 uA.  If the statement is, instead, corrected to set arduinoRxPin instead of rxPin,
		// the idle current goes up to 500 uA.
	//	_configure_pin_tc(rxPin, PIO_INPUT, PIO_DEFAULT);       // Rx



		_configure_pin_tc(arduinoTxPin, PIO_OUTPUT_0, PIO_DEFAULT);    // Force Tx low always (it is an input to the inverter IC whose power is toggled)  // OK 10.12.19
		_configure_pin_tc(GPS_1PPS, PIO_OUTPUT_0, PIO_DEFAULT);
		_configure_pin_tc(GPS_EXTINT, PIO_OUTPUT_0, PIO_DEFAULT);
		_configure_pin_tc(GPS_NRESET, PIO_OUTPUT_0, PIO_DEFAULT);  // NRESET at zero because device is powered down
		_configure_pin_tc(GPS_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		break;
	case ACTION_ENABLE:
		_configure_pin_tc(arduinoRxPin, PIO_PERIPH_A, PIO_DEFAULT);    // Rx  // OK 10.12.19
		_configure_pin_tc(arduinoTxPin, PIO_PERIPH_A, PIO_DEFAULT);    // Force Tx low always (it is an input to the inverter IC whose power is toggled)  // OK 10.12.19
		_configure_pin_tc(GPS_1PPS, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(GPS_EXTINT, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(GPS_NRESET, PIO_OUTPUT_1, PIO_DEFAULT);
		_configure_pin_tc(GPS_EN, PIO_OUTPUT_1, PIO_DEFAULT);
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		// Reset Tx and Rx state machines; disable them; reset status bits
		USART1->US_CR = UART_CR_RSTRX | UART_CR_RSTTX | UART_CR_RXDIS | UART_CR_TXDIS | UART_CR_RSTSTA;
		PIOA->PIO_CODR = rxPin;                                   // Rx - this DOES set the pin low  // OK 10.12.19
		PIOA->PIO_OER = rxPin;
		PIOA->PIO_PER = rxPin;
		PIOA->PIO_CODR = txPin;                                   // Tx - this DOES set the pin low  // OK 10.12.19
		PIOA->PIO_OER = txPin;
		PIOA->PIO_PER = txPin;
		_configure_pin_tc(GPS_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		// OK to sleep with GPS enabled
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		// OK to sleep with GPS enabled
		break;
	case ACTION_WAKE_FROM_SLEEP:
		// Never any action here -- the driver handles explicit enabling and disabling
		break;
	default:
		break;
	}
}


// USART3 / Serial3
void _set_power_sdi12(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	uint32_t rxPin = PIO_PD5;  // <-- stomping on Arduino pin 16 (GPS TxD) if used as an Arduino pin
	uint32_t txPin = PIO_PD4;  // <-- stomping on Arduino pin 8  (SDI-12 power enable pin) if used as an Arduino pin

	uint32_t arduinoRxPin = SDI12_COMM_RXD;
	uint32_t arduinoTxPin = SDI12_COMM_TXD;

	switch (action)
	{
	case ACTION_RESET:
		// _configure_pin(PINS_USART3);
		_configure_pin_tc(arduinoRxPin, PIO_INPUT, PIO_DEFAULT);           // Rx  // OK 10.12.19
		_configure_pin_tc(arduinoTxPin, PIO_OUTPUT_0, PIO_DEFAULT);        // Force Tx low always (it is an input to the inverter IC whose power is toggled)  // OK 10.12.19
		_configure_pin_tc(SDI12_NRESET, PIO_OUTPUT_0, PIO_DEFAULT);   // NRESET at zero because device is powered down
		_configure_pin_tc(SDI12_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		_configure_pin_tc(SDI12_POWER_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		break;
	case ACTION_ENABLE:
		_configure_pin_tc(arduinoRxPin, PIO_PERIPH_B, PIO_DEFAULT);        // Rx  // OK 10.12.19
		_configure_pin_tc(arduinoTxPin, PIO_PERIPH_B, PIO_DEFAULT);        // Tx  // OK 10.12.19
		_configure_pin_tc(SDI12_EN, PIO_OUTPUT_1, PIO_DEFAULT);
		_configure_pin_tc(SDI12_POWER_EN, PIO_OUTPUT_1, PIO_DEFAULT);
		delay(1000);
		_configure_pin_tc(SDI12_NRESET, PIO_OUTPUT_1, PIO_DEFAULT);
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		// Reset Tx and Rx state machines; disable them; reset status bits
		USART3->US_CR = UART_CR_RSTRX | UART_CR_RSTTX | UART_CR_RXDIS | UART_CR_TXDIS | UART_CR_RSTSTA;
		PIOD->PIO_CODR = rxPin;                                         // Rx - this DOES set the pin low  // OK 10.12.19
		PIOD->PIO_OER = rxPin;
		PIOD->PIO_PER = rxPin;
		PIOD->PIO_CODR = txPin;                                         // Tx - this DOES set the pin low  // OK 10.12.19
		PIOD->PIO_OER = txPin;
		PIOD->PIO_PER = txPin;
		_configure_pin_tc(SDI12_NRESET, PIO_OUTPUT_0, PIO_DEFAULT);   // NRESET at zero because device is powered down
		_configure_pin_tc(SDI12_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		_configure_pin_tc(SDI12_POWER_EN, PIO_OUTPUT_0, PIO_DEFAULT);
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		// OK to sleep with SDI-12 enabled
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		// OK to sleep with SDI-12 enabled
		break;
	case ACTION_WAKE_FROM_SLEEP:
		// Never any action here -- the driver handles explicit enabling and disabling
		break;
	default:
		break;
	}
}


void _set_power_sd_card(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	switch (action)
	{
	case ACTION_RESET:
		_configure_pin_t(SD_EN,  PIO_OUTPUT_0);
		_configure_pin_tc(MCCDA, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCCK,  PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCDA0, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCDA1, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCDA2, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCDA3, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_t(SD_DETECT, PIO_OUTPUT_0);
		break;
	case ACTION_ENABLE:
		// power up the card
		_configure_pin_t(SD_EN,  PIO_OUTPUT_1);
		_configure_pin_tc(MCCDA, PIO_PERIPH_A, PIO_PULLUP);
		_configure_pin_tc(MCCK,  PIO_PERIPH_A, PIO_DEFAULT);
		_configure_pin_tc(MCDA0, PIO_PERIPH_A, PIO_PULLUP);
		_configure_pin_tc(MCDA1, PIO_PERIPH_A, PIO_PULLUP);
		_configure_pin_tc(MCDA2, PIO_PERIPH_A, PIO_PULLUP);
		_configure_pin_tc(MCDA3, PIO_PERIPH_A, PIO_PULLUP);
		_configure_pin_tc(SD_DETECT, PIO_INPUT, PIO_DEFAULT);
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		_configure_pin_tc(MCCDA, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCCK,  PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCDA0, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCDA1, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCDA2, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_tc(MCDA3, PIO_INPUT, PIO_DEFAULT);
		_configure_pin_t(SD_EN,  PIO_OUTPUT_0);
		_configure_pin_t(SD_DETECT, PIO_OUTPUT_0);
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
//		_configure_pin_tc(MCCDA, PIO_INPUT, PIO_DEFAULT);
//		_configure_pin_tc(MCCK,  PIO_INPUT, PIO_DEFAULT);
//		_configure_pin_tc(MCDA0, PIO_INPUT, PIO_DEFAULT);
//		_configure_pin_tc(MCDA1, PIO_INPUT, PIO_DEFAULT);
//		_configure_pin_tc(MCDA2, PIO_INPUT, PIO_DEFAULT);
//		_configure_pin_tc(MCDA3, PIO_INPUT, PIO_DEFAULT);
//		_configure_pin_t(SD_EN, PIO_OUTPUT_0);
//		_configure_pin_t(SD_DETECT, PIO_OUTPUT_0);
		_configure_pin_t(MCCDA, PIO_OUTPUT_0);
		_configure_pin_t(MCCK,  PIO_OUTPUT_0);
		_configure_pin_t(MCDA0, PIO_OUTPUT_0);
		_configure_pin_t(MCDA1, PIO_OUTPUT_0);
		_configure_pin_t(MCDA2, PIO_OUTPUT_0);
		_configure_pin_t(MCDA3, PIO_OUTPUT_0);
		_configure_pin_t(SD_EN,  PIO_OUTPUT_0);
		_configure_pin_t(SD_DETECT, PIO_OUTPUT_0);
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		_configure_pin_t(MCCDA, PIO_OUTPUT_0);
		_configure_pin_t(MCCK,  PIO_OUTPUT_0);
		_configure_pin_t(MCDA0, PIO_OUTPUT_0);
		_configure_pin_t(MCDA1, PIO_OUTPUT_0);
		_configure_pin_t(MCDA2, PIO_OUTPUT_0);
		_configure_pin_t(MCDA3, PIO_OUTPUT_0);
		_configure_pin_t(SD_EN,  PIO_OUTPUT_0);
		_configure_pin_t(SD_DETECT, PIO_OUTPUT_0);
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{
			_configure_pin_t(SD_EN,  PIO_OUTPUT_1);
			_configure_pin_tc(MCCDA, PIO_PERIPH_A, PIO_PULLUP);
			_configure_pin_tc(MCCK,  PIO_PERIPH_A, PIO_DEFAULT);
			_configure_pin_tc(MCDA0, PIO_PERIPH_A, PIO_PULLUP);
			_configure_pin_tc(MCDA1, PIO_PERIPH_A, PIO_PULLUP);
			_configure_pin_tc(MCDA2, PIO_PERIPH_A, PIO_PULLUP);
			_configure_pin_tc(MCDA3, PIO_PERIPH_A, PIO_PULLUP);
			_configure_pin_tc(SD_DETECT, PIO_INPUT, PIO_DEFAULT);
		}
		break;
	default:
		break;
	}
}


void _set_power_usb(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	switch (action)
	{
	case ACTION_RESET:
		// UOTGHS_CTRL.USBE <- 0 to disable the USBOTG module: Datasheet sec. 39.5
		otg_disable();
		_configure_pin(PINS_USB);

		break;
	case ACTION_ENABLE:
		_configure_pin_t(PINS_USB, PIO_PERIPH_A);
		otg_enable();
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		otg_disable();
		_configure_pin(PINS_USB);
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		otg_disable();
//		_configure_pin(PINS_USB);
		_configure_pin_t(PINS_USB, PIO_OUTPUT_0);
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		otg_disable();
//		_configure_pin(PINS_USB);
		_configure_pin_t(PINS_USB, PIO_OUTPUT_0);
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{
			_configure_pin_t(PINS_USB, PIO_PERIPH_A);
			otg_enable();
		}
		break;
	default:
		break;
	}
}

// CAN pins are not connected, so not much value here
void _set_power_can(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	switch (action)
	{
	case ACTION_RESET:
		_configure_pin(DAC0);  // instead of the CAN peripheral, just output 0
		_configure_pin(DAC1);  // instead of the CAN peripheral, just output 0
		break;
	case ACTION_ENABLE:
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		_configure_pin_t(DAC0, PIO_OUTPUT_0);
		_configure_pin_t(DAC1, PIO_OUTPUT_0);
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		_configure_pin_t(DAC0, PIO_OUTPUT_0);
		_configure_pin_t(DAC1, PIO_OUTPUT_0);
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{

		}
		break;
	default:
		break;
	}
}


void _set_power_analog(gpio_action_t action)
{
	static bool enabled_p __attribute__((unused)) = false;
	switch (action)
	{
	case ACTION_RESET:
		_configure_pin(A1);
		_configure_pin(A2);
		_configure_pin(A3);
		_configure_pin(A4);
		break;
	case ACTION_ENABLE:
		// Initialize Analog Controller
		pmc_enable_periph_clk(ID_ADC);
		adc_init(ADC, SystemCoreClock, ADC_FREQ_MAX, ADC_STARTUP_FAST);
		adc_configure_timing(ADC, 0, ADC_SETTLING_TIME_3, 1);
		adc_configure_trigger(ADC, ADC_TRIG_SW, 0); // Disable hardware trigger.
		adc_disable_interrupt(ADC, 0xFFFFFFFF); // Disable all ADC interrupts.
		adc_disable_all_channel(ADC);
		// Initialize analogOutput module
		analogOutputInit();
		enabled_p = true;
		break;
	case ACTION_DISABLE:
		enabled_p = false;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		_configure_pin_t(A1, PIO_OUTPUT_0);
		_configure_pin_t(A2, PIO_OUTPUT_0);
		_configure_pin_t(A3, PIO_OUTPUT_0);
		_configure_pin_t(A4, PIO_OUTPUT_0);
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		_configure_pin_t(A1, PIO_OUTPUT_0);
		_configure_pin_t(A2, PIO_OUTPUT_0);
		_configure_pin_t(A3, PIO_OUTPUT_0);
		_configure_pin_t(A4, PIO_OUTPUT_0);
		break;
	case ACTION_WAKE_FROM_SLEEP:
		if (enabled_p)
		{

		}
		break;
	default:
		break;
	}
}


void _record_power_state(ccsg_power_state_history_t *var, gpio_action_t action)
{
	switch(action)
	{
	case ACTION_RESET:
		var->current = POWER_STATE_RESET;
		var->preSleep = POWER_STATE_UNKNOWN;
		break;
	case ACTION_ENABLE:
		var->current = POWER_STATE_ENABLED;
		var->preSleep = POWER_STATE_UNKNOWN;
		break;
	case ACTION_DISABLE:
		var->current = POWER_STATE_DISABLED;
		var->preSleep = POWER_STATE_UNKNOWN;
		break;
	case ACTION_PREPARE_TO_SLEEP:
		var->preSleep = var->current;
		var->current = POWER_STATE_PREPARED_FOR_SLEEP;
		break;
	case ACTION_ALT_PREPARE_TO_SLEEP:
		var->preSleep = var->current;
		var->current = POWER_STATE_ALT_PREPARED_FOR_SLEEP;
		break;
	case ACTION_WAKE_FROM_SLEEP:
		// **FIXME** test preSleep to make sure it is NOT POWER_STATE_UNKNOWN
		var->current = var->preSleep;
		var->preSleep = POWER_STATE_UNKNOWN;
		break;
	default:
		var->current = POWER_STATE_UNKNOWN;
		var->preSleep = POWER_STATE_UNKNOWN;
		break;
	}
}


void _print_power_state(char* name, ccsg_power_states_t state)
{
	CCSG_DEBUG_PRINTF("%s ", name);
	switch (state)
	{
	case POWER_STATE_UNKNOWN:
		CCSG_DEBUG_PRINT("UNK ");
		break;
	case POWER_STATE_RESET:
		CCSG_DEBUG_PRINT("RST ");
		break;
	case POWER_STATE_ENABLED:
		CCSG_DEBUG_PRINT("ENA ");
		break;
	case POWER_STATE_DISABLED:
		CCSG_DEBUG_PRINT("DIS ");
		break;
	case POWER_STATE_PREPARED_FOR_SLEEP:
		CCSG_DEBUG_PRINT("P4S ");
		break;
	case POWER_STATE_ALT_PREPARED_FOR_SLEEP:
		CCSG_DEBUG_PRINT("A4S ");
		break;
	default:
		CCSG_DEBUG_PRINT("??? ");
		break;
	}
}

// --------------------------------------- External interface ---------------------------------------


void ccsg_power_set_state(ccsg_power_device_t device, gpio_action_t action)
{
	switch(device)
	{
	case DEVICE_LEDS:
		_set_power_leds(action);
		_record_power_state(&ccsg_power_state.leds, action);
		break;
	case DEVICE_ACCELEROMETER:
		_set_power_accelerometer(action);
		_record_power_state(&ccsg_power_state.accelerometer, action);
		break;
	case DEVICE_HUMIDITY_SENSOR:
		_set_power_humidity_sensor(action);
		_record_power_state(&ccsg_power_state.humidity_sensor, action);
		break;
	case DEVICE_LORA:
		_set_power_lora(action);
		_record_power_state(&ccsg_power_state.lora, action);
		break;
	case DEVICE_COULOMB_COUNTER:
		_set_power_coulomb_counter(action);
		_record_power_state(&ccsg_power_state.coulomb_counter, action);
		break;
#if ENVISENSE_SERIAL == ENVISENSE_BLUETOOTH
	case DEVICE_BLUETOOTH:
		_set_power_bluetooth(action);
		_record_power_state(&ccsg_power_state.bluetooth, action);
		break;
#elif ENVISENSE_SERIAL == ENVISENSE_DEBUG
	case DEVICE_DEBUG:
		_set_power_debug(action);
		_record_power_state(&ccsg_power_state.debug, action);
		break;
#endif
	case DEVICE_I2C:
		_set_power_i2c(action);
		_record_power_state(&ccsg_power_state.i2c, action);
		break;
	case DEVICE_ULTRASONIC:
		_set_power_ultrasonic(action);
		if (action == ACTION_ENABLE)
			CCSG_DEBUG_PRINTLN("[power   ] Recording power state");
		_record_power_state(&ccsg_power_state.ultrasonic, action);
		if (action == ACTION_ENABLE)
			CCSG_DEBUG_PRINTLN("[power   ] ccsg_power_set_state() complete");
		break;
	case DEVICE_GPS:
		_set_power_gps(action);
		_record_power_state(&ccsg_power_state.gps, action);
		break;
	case DEVICE_SDI12:
		_set_power_sdi12(action);
		_record_power_state(&ccsg_power_state.sdi12, action);
		break;
	case DEVICE_SD_CARD:
		_set_power_sd_card(action);
		_record_power_state(&ccsg_power_state.sd_card, action);
		break;
	case DEVICE_USB:
		_set_power_usb(action);
		_record_power_state(&ccsg_power_state.usb, action);
		break;
	case DEVICE_CAN:
		_set_power_can(action);
		_record_power_state(&ccsg_power_state.can, action);
		break;
	case DEVICE_ANALOG:
		_set_power_analog(action);
		_record_power_state(&ccsg_power_state.analog, action);
		break;
	default:
		// **FIXME** flag an error
		break;
	}
}


void ccsg_power_print_state(ccsg_power_device_t device)
{
	CCSG_DEBUG_PRINT(F("[power   ] "));

	switch(device)
	{
	case DEVICE_LEDS:
		_print_power_state((char*)"LEDs", ccsg_power_state.leds.current);
		break;
	case DEVICE_ACCELEROMETER:
		_print_power_state((char*)"Accelerometer", ccsg_power_state.accelerometer.current);
		break;
	case DEVICE_HUMIDITY_SENSOR:
		_print_power_state((char*)"Humidity", ccsg_power_state.humidity_sensor.current);
		break;
	case DEVICE_LORA:
		_print_power_state((char*)"LoRa", ccsg_power_state.lora.current);
		break;
	case DEVICE_COULOMB_COUNTER:
		_print_power_state((char*)"Coulomb", ccsg_power_state.coulomb_counter.current);
		break;
#if ENVISENSE_SERIAL == ENVISENSE_BLUETOOTH
	case DEVICE_BLUETOOTH:
		_print_power_state("Bluetooth", ccsg_power_state.bluetooth.current);
		break;
#elif ENVISENSE_SERIAL == ENVISENSE_DEBUG
	case DEVICE_DEBUG:
		_print_power_state((char*)"Debug", ccsg_power_state.debug.current);
		break;
#endif
	case DEVICE_I2C:
		_print_power_state((char*)"I2C", ccsg_power_state.i2c.current);
		break;
	case DEVICE_ULTRASONIC:
		_print_power_state((char*)"Ultrasonic", ccsg_power_state.ultrasonic.current);
		break;
	case DEVICE_GPS:
		_print_power_state((char*)"GPS", ccsg_power_state.gps.current);
		break;
	case DEVICE_SDI12:
		_print_power_state((char*)"SDI12", ccsg_power_state.sdi12.current);
		break;
	case DEVICE_SD_CARD:
		_print_power_state((char*)"SD Card", ccsg_power_state.sd_card.current);
		break;
	case DEVICE_USB:
		_print_power_state((char*)"USB", ccsg_power_state.usb.current);
		break;
	case DEVICE_CAN:
		_print_power_state((char*)"CAN", ccsg_power_state.can.current);
		break;
	case DEVICE_ANALOG:
		_print_power_state((char*)"Analog", ccsg_power_state.analog.current);
		break;
	default:
		// **FIXME** flag an error
		break;
	}
	CCSG_DEBUG_PRINTLN("");
}


void ccsg_power_reset_all_gpio_pins()
{
	// 2019-0721 Set all GPIOs initially to output low.
	// **FIXME** May be slightly safer to do this more selectively
	PIO_SetOutput(PIOA, 0xFFFFFFFF, LOW, 0, 0);
	PIO_SetOutput(PIOB, 0xFFFFFFFF, LOW, 0, 0);
	PIO_SetOutput(PIOC, 0xFFFFFFFF, LOW, 0, 0);
	PIO_SetOutput(PIOD, 0xFFFFFFFF, LOW, 0, 0);
}


void ccsg_power_set_state_all(gpio_action_t action)
{
	ccsg_power_set_state(DEVICE_LEDS, action);
	ccsg_power_set_state(DEVICE_I2C, action);
	ccsg_power_set_state(DEVICE_COULOMB_COUNTER, action);
	ccsg_power_set_state(DEVICE_SDI12, action);
	ccsg_power_set_state(DEVICE_SD_CARD, action);
	ccsg_power_set_state(DEVICE_ACCELEROMETER, action);
	ccsg_power_set_state(DEVICE_HUMIDITY_SENSOR, action);
#if ENVISENSE_SERIAL == ENVISENSE_BLUETOOTH
	ccsg_power_set_state(DEVICE_BLUETOOTH, action);
#elif ENVISENSE_SERIAL == ENVISENSE_DEBUG
	ccsg_power_set_state(DEVICE_DEBUG, action);
#endif
	ccsg_power_set_state(DEVICE_GPS, action);
    ccsg_power_set_state(DEVICE_LORA, action);
	ccsg_power_set_state(DEVICE_ULTRASONIC, action);
	ccsg_power_set_state(DEVICE_USB, action);
	ccsg_power_set_state(DEVICE_CAN, action);
	ccsg_power_set_state(DEVICE_ANALOG, action);
}


void _ccsg_power_restore_state(ccsg_power_device_t device, ccsg_power_states_t priorDeviceState)
{
	switch (priorDeviceState)
	{
	case POWER_STATE_UNKNOWN:
		ccsg_power_set_state(device, ACTION_RESET);
		break;
	case POWER_STATE_RESET:
		ccsg_power_set_state(device, ACTION_RESET);
		break;
	case POWER_STATE_ENABLED:
		ccsg_power_set_state(device, ACTION_ENABLE);
		break;
	case POWER_STATE_DISABLED:
		ccsg_power_set_state(device, ACTION_DISABLE);
		break;
	case POWER_STATE_PREPARED_FOR_SLEEP:
		CCSG_DEBUG_PRINTLN("[power   ] Restoring PREPARED_FOR_SLEEP state -- probably not what you wanted");
		ccsg_power_set_state(device, ACTION_PREPARE_TO_SLEEP);
		break;
	case POWER_STATE_ALT_PREPARED_FOR_SLEEP:
		CCSG_DEBUG_PRINTLN("[power   ] Restoring ALT_PREPARED_FOR_SLEEP state -- probably not what you wanted");
		ccsg_power_set_state(device, ACTION_ALT_PREPARE_TO_SLEEP);
		break;
	default:
		CCSG_DEBUG_PRINTLN("[power   ] ERROR: tried to restore an invalid power state");
		break;
	}
}


void ccsg_power_restore_state_all(ccsg_power_state_t priorState)
{
	_ccsg_power_restore_state(DEVICE_LEDS, priorState.leds.current);
	_ccsg_power_restore_state(DEVICE_I2C, priorState.i2c.current);
	_ccsg_power_restore_state(DEVICE_COULOMB_COUNTER, priorState.coulomb_counter.current);
	_ccsg_power_restore_state(DEVICE_SDI12, priorState.sdi12.current);
	_ccsg_power_restore_state(DEVICE_SD_CARD, priorState.sd_card.current);
	_ccsg_power_restore_state(DEVICE_ACCELEROMETER, priorState.accelerometer.current);
	_ccsg_power_restore_state(DEVICE_HUMIDITY_SENSOR, priorState.humidity_sensor.current);
#if ENVISENSE_SERIAL == ENVISENSE_BLUETOOTH
	_ccsg_power_restore_state(DEVICE_BLUETOOTH, priorState.bluetooth.current);
#elif ENVISENSE_SERIAL == ENVISENSE_DEBUG
	_ccsg_power_restore_state(DEVICE_DEBUG, priorState.debug.current);
#endif
	_ccsg_power_restore_state(DEVICE_GPS, priorState.gps.current);
	_ccsg_power_restore_state(DEVICE_LORA, priorState.lora.current);
	_ccsg_power_restore_state(DEVICE_ULTRASONIC, priorState.ultrasonic.current);
	_ccsg_power_restore_state(DEVICE_USB, priorState.usb.current);
	_ccsg_power_restore_state(DEVICE_CAN, priorState.can.current);
	_ccsg_power_restore_state(DEVICE_ANALOG, priorState.analog.current);
}


void ccsg_power_print_state_all()
{
	ccsg_power_print_state(DEVICE_LEDS);
	ccsg_power_print_state(DEVICE_I2C);
	ccsg_power_print_state(DEVICE_COULOMB_COUNTER);
	ccsg_power_print_state(DEVICE_SDI12);
	ccsg_power_print_state(DEVICE_SD_CARD);
	ccsg_power_print_state(DEVICE_ACCELEROMETER);
	ccsg_power_print_state(DEVICE_HUMIDITY_SENSOR);
#if ENVISENSE_SERIAL == ENVISENSE_BLUETOOTH
	ccsg_power_print_state(DEVICE_BLUETOOTH);
#elif ENVISENSE_SERIAL == ENVISENSE_DEBUG
	ccsg_power_print_state(DEVICE_DEBUG);
#endif
	ccsg_power_print_state(DEVICE_GPS);
    ccsg_power_print_state(DEVICE_LORA);
	ccsg_power_print_state(DEVICE_ULTRASONIC);
	ccsg_power_print_state(DEVICE_USB);
	ccsg_power_print_state(DEVICE_CAN);
	ccsg_power_print_state(DEVICE_ANALOG);
}

#ifdef __cplusplus
}
#endif
