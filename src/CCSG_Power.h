/*******************************************************************************
 * CCSG Board Support
 *
 * EnviSense Power Management
 *
 * Copyright (c) 2019, 2020 by Bob Iannucci
 *******************************************************************************/

#ifndef __CCSG_POWER_H__
#define __CCSG_POWER_H__

#include "variant.h"
#include "CCSG_BoardConfig.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef enum gpio_action_t
{
	ACTION_RESET,
	ACTION_ENABLE,
	ACTION_DISABLE,
	ACTION_PREPARE_TO_SLEEP,     // Primarily INPUT
	ACTION_ALT_PREPARE_TO_SLEEP, // Primarily OUTPUT_0
	ACTION_WAKE_FROM_SLEEP
} gpio_action_t;


typedef enum ccsg_power_device_t
{
	DEVICE_LEDS,
	DEVICE_ACCELEROMETER,
	DEVICE_HUMIDITY_SENSOR,
	DEVICE_LORA,
	DEVICE_COULOMB_COUNTER,
#if ENVISENSE_SERIAL == ENVISENSE_BLUETOOTH
	DEVICE_BLUETOOTH,
#elif ENVISENSE_SERIAL == ENVISENSE_DEBUG
	DEVICE_DEBUG,
#endif
	DEVICE_I2C,
	DEVICE_ULTRASONIC,
	DEVICE_GPS,
	DEVICE_SDI12,
	DEVICE_SD_CARD,
	DEVICE_USB,
	DEVICE_CAN,
	DEVICE_ANALOG,
} ccsg_power_device_t;


typedef enum ccsg_power_states_t
{
	POWER_STATE_UNKNOWN,
	POWER_STATE_RESET,
	POWER_STATE_ENABLED,
	POWER_STATE_DISABLED,
	POWER_STATE_PREPARED_FOR_SLEEP,
	POWER_STATE_ALT_PREPARED_FOR_SLEEP
} ccsg_power_states_t;


typedef struct ccsg_power_state_history_t
{
	ccsg_power_states_t current;
	ccsg_power_states_t preSleep;
} ccsg_power_state_history_t;


typedef struct ccsg_power_state_t
{
	ccsg_power_state_history_t leds;
	ccsg_power_state_history_t accelerometer;
	ccsg_power_state_history_t humidity_sensor;
	ccsg_power_state_history_t lora;
	ccsg_power_state_history_t coulomb_counter;
#if ENVISENSE_SERIAL == ENVISENSE_BLUETOOTH
	ccsg_power_state_history_t bluetooth;
#elif ENVISENSE_SERIAL == ENVISENSE_DEBUG
	ccsg_power_state_history_t debug;
#endif
	ccsg_power_state_history_t i2c;
	ccsg_power_state_history_t ultrasonic;
	ccsg_power_state_history_t gps;
	ccsg_power_state_history_t sdi12;
	ccsg_power_state_history_t sd_card;
	ccsg_power_state_history_t usb;
	ccsg_power_state_history_t can;
	ccsg_power_state_history_t analog;
} ccsg_power_state_t;


extern void ccsg_power_set_state(ccsg_power_device_t device, gpio_action_t action);
extern void ccsg_power_set_state_all(gpio_action_t action);
extern void ccsg_power_restore_state_all(ccsg_power_state_t priorState);
extern void ccsg_power_reset_all_gpio_pins();
extern void ccsg_power_print_state_all();

#ifdef __cplusplus
}
#endif

#endif
